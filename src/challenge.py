import numpy as np
import math
import random

def rosenbrock(pos):
    return (1-pos[0])**2 + (pos[1]-pos[0]**2)**2

def numerical_derivative_simple(func, pos, step, index):
    # Gradient computations are more precise when performed over three consecutive points, rather than two
    preceding_point = np.copy(pos)
    succeding_point = np.copy(pos)
    preceding_point[index] -= step
    succeding_point[index] += step
    # Compute for that index only
    derivative = (func(succeding_point) - func(preceding_point))/(2*step)
    return derivative

def rmsprop_seq(func, pos, nb_epochs, grad_square_decay_factor, rate, step):
    epsilon = 1e-8
    grad_avg = np.empty(len(pos))
    grad_avg.fill(0)
    trajectory = []
    distances = []
    with open('rosenbrock_coord_serial.csv', 'w') as filehandle:
        for epoch in range(nb_epochs):
            for index, value in enumerate(pos):
                val_grad = numerical_derivative_simple(func, pos, step, index)
                grad_avg[index] = grad_square_decay_factor * grad_avg[index] + (1 - grad_square_decay_factor) * val_grad**2
                pos[index] -= (rate / math.sqrt(grad_avg[index] + epsilon)) * val_grad
            # For visualization purpose
            trajectory.append(np.copy(pos))
            distance_to_target = math.sqrt((pos[0] - 1)**2 + (pos[1] - 1)**2)
            distances.append(distance_to_target)
            print(f'{pos[0]}, {pos[1]}', file=filehandle)
    return distance_to_target, pos, trajectory, distances

def show_distance(distances):
    plt.plot(distances)
    plt.show()

def show_trace(trajectory):
    x = [pos[0] for pos in trajectory]
    y = [pos[1] for pos in trajectory]
    plt.plot(x,y)
    plt.gca().set_aspect("equal")
    plt.show()

def validate(learning_rate, grad_step, nb_steps):
    max_distance = 0
    min_distance = 1000
    min_pos = [-1000,-1000]
    max_pos = [-1000,-1000]
    close = 0
    # randomly select 20 random positions
    for i in range(20):
        x = random.randint(-5,5)
        y = random.randint(-5,5)
        distance_to_target, pos, trajectory, distances = rmsprop_seq(rosenbrock,[x,y],nb_steps,0.99,learning_rate,grad_step)
        if distance_to_target >= max_distance:
            max_distance = distance_to_target
            max_pos = pos
        if distance_to_target < min_distance:
            min_distance = distance_to_target
            min_pos = pos
        # what percentage of results are within 10% of the optimum?
        if distance_to_target < 0.1:
            close += 1
    return max_distance, max_pos, 100*close/20.0, min_distance, min_pos

def optimize_parameters():
    initial_grad = 0.000001
    best_min_distance = 1000
    best_percent_close = 0
    best_percent_learn = 0
    best_percent_grad = 0
    # for all tested gradients
    for i in range(7):
        current_grad = initial_grad / 10**i
        min_worst_distance_over_gradient = 1000
        print()
        print("grad",current_grad)
        current_learn = 0.9
        # and all tested learning rates
        for j in range(15):
            print("learn",current_learn)
            # compute worst and best results over a set of random tries
            worst_distance, worst_pos, percent_close, best_distance, best_pos = validate(current_learn, current_grad, 10000)
            # what is the learning rate that gives the minimum of the worst distance to target over a set of random tries?
            if worst_distance < min_worst_distance_over_gradient:
                min_worst_distance_over_gradient = worst_distance
                best_pos_over_gradient = worst_pos
                best_grad_over_gradient = current_grad
                best_learn_over_gradient = current_learn
            if percent_close > best_percent_close:
                best_percent_close = percent_close
                best_percent_learn = current_learn
                best_percent_grad = current_grad
            current_learn = (current_learn + 9)/10
        # do we have the best pair of parameters yet?
        if min_worst_distance_over_gradient < best_min_distance:
            best_min_distance = min_worst_distance_over_gradient
            best_grad = best_grad_over_gradient
            best_rate = best_learn_over_gradient
            best_worst_pos = best_pos_over_gradient
        print("Current Best rate is", best_rate,"best gradient step is", best_grad,"minimal maximal distance of",best_min_distance,"pos",best_worst_pos)
        print("Best percent close is",best_percent_close,"for rate",best_percent_learn,"and gradient",best_percent_grad)
    print("Overall Best rate is", best_rate,"best gradient step is", best_grad,"maximal distance of",best_min_distance,"pos",best_worst_pos)
    print("Overall best percent close is",best_percent_close,"for rate",best_percent_learn,"and gradient",best_percent_grad)
    return best_rate, best_grad

def main():
    print("SAT Challenge")
    # Find optimal parameters
    # best_rate, best_grad = optimize_parameters()
    x = random.randint(-5,5)
    y = random.randint(-5,5)
    # Produce one CSV with optimal parameters and random starting location
    rmsprop_seq(rosenbrock,[x,y],10000,0.99,0.9,1e-10)

if __name__ == "__main__":
    main()